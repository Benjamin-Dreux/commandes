package com.duPainDansLesVoiles.commandes.converters;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToLocalDateConverter implements Converter<String, LocalDate> {

    @Override
    public LocalDate convert(String source) {
      if (source == null) {
        return null;
      }
      return LocalDate.parse(source);
    }
}
