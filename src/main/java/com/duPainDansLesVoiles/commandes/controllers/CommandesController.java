package com.duPainDansLesVoiles.commandes.controllers;


import  java.util.stream.Stream;
import com.duPainDansLesVoiles.commandes.model.Commande;
import com.duPainDansLesVoiles.commandes.model.ItemCommande;
import com.duPainDansLesVoiles.commandes.model.Produit;
import com.duPainDansLesVoiles.commandes.repository.CommandeRepository;
import com.duPainDansLesVoiles.commandes.repository.ProduitRepository;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import static  java.util.stream.Collectors.toList;

@Controller
public class CommandesController {

  @Autowired CommandeRepository commandeRepository;
  @Autowired ProduitRepository produitRepository;

  @GetMapping("/")
  public String home(){
    return "redirect:/commandes";
  }

  @GetMapping("/commandes")
  public String list(ModelMap model){
    var commandes = commandeRepository.findAll();
    var produits = produitRepository.findAll();

    model.addAttribute("commandes", commandes);

    return "commandes/list";
  }

  @GetMapping("/commandes/new")
  public String newForm(ModelMap model){
    var produitsList = produitRepository.findAll();
    var produits = StreamSupport.stream(produitsList.spliterator(), false);

    var produitsByCategory = produits.collect(Collectors.groupingBy(Produit::getCategorie));

    model
    .addAttribute("commande", new CommandeForm(produitsList))
    .addAttribute("produitsByCategory", produitsByCategory);
    return "commandes/new";
  }

  @PostMapping("/commandes/new")
  public String addCommande(CommandeForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes){
    if(bindingResult.hasErrors()){
      System.out.println(String.format("Errors"));
      System.out.println(String.format("Errors %s", bindingResult));
      redirectAttributes
      .addFlashAttribute("commande", form)
      .addFlashAttribute("org.springframework.validation.BindingResult.commande", bindingResult);
      return "redirect:/commandes/new";
    }
    var commande = form.toCommande();
    commandeRepository.save(commande);
    return "redirect:/commandes";

  }

  @GetMapping("/commandes/{commandeId}")
  public String showCommande(@PathVariable Integer commandeId, ModelMap model){
    var commande = commandeRepository.findById(commandeId);
    if (commande.isPresent()){
      model.addAttribute("commande", commande.get());
      return "commandes/edit";
    } else {
      return "404";
    }
  }

  @PostMapping("/commandes/{commandeId}")
  public String editCommande(CommandeForm form, BindingResult bindingResult){
    var commande = form.toCommande();
    commandeRepository.save(commande);
    return "redirect:/commandes";
  }
}


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
class CommandeForm {
  private Integer id;
  private String nomClient;
  private String numeroTelephone;
  private LocalDate dateCommande;
  private String numeroClient;
  private List<ItemCommande> items;

  CommandeForm(Iterable<Produit> produitsList){
    var produits = StreamSupport.stream(produitsList.spliterator(), false);
    items = produits
    .map( (Produit p) ->
      ItemCommande
      .builder()
      .produitId(p.getId())
      .quantite(0)
      .build()
    )
    .collect(toList());
  }

  Commande toCommande(){
    return Commande.builder()
    .id(id)
    .nomClient(nomClient)
    .numeroTelephone(numeroTelephone)
    .dateCommande(dateCommande)
    .numeroClient(numeroClient)
    .items(items)
    .build();
  }

}
