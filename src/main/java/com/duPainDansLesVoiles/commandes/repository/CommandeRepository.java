package com.duPainDansLesVoiles.commandes.repository;

import org.springframework.data.repository.CrudRepository;
import com.duPainDansLesVoiles.commandes.model.Commande;

public interface CommandeRepository extends CrudRepository<Commande, Integer> {
}
