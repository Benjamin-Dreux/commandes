package com.duPainDansLesVoiles.commandes.repository;

import org.springframework.data.repository.CrudRepository;
import com.duPainDansLesVoiles.commandes.model.Produit;

public interface ProduitRepository extends CrudRepository<Produit, Integer> {
}
