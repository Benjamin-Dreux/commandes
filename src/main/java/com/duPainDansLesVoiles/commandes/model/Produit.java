package com.duPainDansLesVoiles.commandes.model;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

import org.springframework.data.annotation.Id;

@Data
@Builder
public class Produit {
  @Id Integer id;
  private String description;
  private String categorie;
  private boolean active;
}
