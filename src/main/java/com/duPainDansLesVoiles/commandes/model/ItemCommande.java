package com.duPainDansLesVoiles.commandes.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemCommande {
  private Integer produitId;
  private Integer quantite;
}
