package com.duPainDansLesVoiles.commandes.model;

import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;

import org.springframework.data.annotation.Id;

@Data
@Builder
public class Commande {
  @Id
  private Integer id;
  private String nomClient;
  private String numeroTelephone;
  private LocalDate dateCommande;
  private String numeroClient;

  private List<ItemCommande> items;

  
}
