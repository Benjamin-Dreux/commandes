alter table commande drop column nb_croissant;
alter table commande drop column nb_chocolatine;
alter table commande drop column nb_brioche_chocolat;

create table item_commande (
  quantite integer,
  produit_id integer,
  commande_key integer
);
